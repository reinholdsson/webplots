
library(Rook)
library(webplots)
library(MASS)

app = function(env) {
  req = Rook::Request$new(env)
  res = Rook::Response$new()

  df <- subset(survey, select = c("Sex", "Exer", "Smoke"))
  res$write(parallel_sets(df))

  res$finish()
}

s = Rhttpd$new()
s$add(name = "app", app = app)
s$start(listen = '127.0.0.1', port = 22221)
s$browse("app")

while(TRUE) Sys.sleep(.Machine$integer.max)
