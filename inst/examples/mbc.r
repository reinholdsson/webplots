
library(Rook)
library(webplots)
library(MASS)

app = function(env) {
    req = Rook::Request$new(env)
    res = Rook::Response$new()
    
    df <- mtcars[c("cyl", "gear", "carb")]
    df <- ddply(df, .(cyl, gear), function(x) mean(x$carb))
    
    df <- rbind(df, c(8,4,0))
    
    res$write(mbc(df, "cyl", "gear", "V1"))
    res$finish()
}

s = Rhttpd$new()
s$add(name = "app", app = app)
s$start(listen = '127.0.0.1', port = 22221)
s$browse("app")

while(TRUE) Sys.sleep(.Machine$integer.max)
