
library(Rook)
library(webplots)
library(MASS)

app = function(env) {
    req = Rook::Request$new(env)
    res = Rook::Response$new()

    #//energy = {"nodes":[
    #    //{"name":"Agricultural 'waste'"},
    #    //{"name":"Bio-conversion"},
    #    //{"name":"Liquid"},
    #    //{"name":"Losses"},
    #    //{"name":"Solid"}
    #    //],
    #            //"links":[
    #                //{"source":0,"target":1,"value":124.729},
    #                //{"source":1,"target":2,"value":0.597},
    #                //{"source":1,"target":3,"value":26.862},
    #                //{"source":1,"target":4,"value":280.322}
    #                //]};
    
    #src <- rep("Inkommet", 100)
    #tgt <- sample(c("Växjö", "Halmstad", "Luleå"), 100, replace = T) 
    
    nodes <- data.frame(name = c("a", "b", "c", "d"))
    links <- data.frame(source = c(0,3,1,1), target = c(2,2,2,3), value = c(10,20,30,40))
    
    res$write(sankey(nodes, links))
    
    res$finish()
}

s = Rhttpd$new()
s$add(name = "app", app = app)
s$start(listen = '127.0.0.1', port = 22221)
s$browse("app")

while(TRUE) Sys.sleep(.Machine$integer.max)
