
library(Rook)
library(webplots)
library(MASS)

app = function(env) {
    req = Rook::Request$new(env)
    res = Rook::Response$new()
    
    df <- head(mtcars[c(1, 2, 5)])
    res$write(mbhc(df, color = c("#33CCFF", "#99FF33", "#FF66FF")))
    res$finish()
}

s = Rhttpd$new()
s$add(name = "app", app = app)
s$start(listen = '127.0.0.1', port = 22221)
s$browse("app")

while(TRUE) Sys.sleep(.Machine$integer.max)
