#' Sankey
#'
#' ...
#'
#' @param nodes data frame
#' @param links data frame
#'
#' @export
#'
sankey <- function(nodes, links) {
    html <- files_to_html(search_files(file.path(system.file(package = "webplots"), "deps", "sankey"), c("js", "html")))
    
    # ADD NODES # to be removed
    nodes_json <- df_to_json_array(nodes)  # to be removed
    html <- sub("<<<NODES>>>", nodes_json, html, fixed = TRUE)  # to be removed
    
    # ADD LINKS
    colnames(links) <- c("source", "target", "value")
    links_json <- df_to_json_array(links)
    html <- sub("<<<LINKS>>>", links_json, html, fixed = TRUE)
    
    return(html)
} 
