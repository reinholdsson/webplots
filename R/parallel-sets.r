#' Parallel Sets
#'
#' ...
#'
#' @param df data frame
#'
#' @export
#'
parallel_sets <- function(df) {
    html <- files_to_html(search_files(file.path(system.file(package = "webplots"), "deps", "parallel-sets"), c("js", "html")))
    
    # ADD COLUMN NAMES
    col_names <- sprintf("\"%s\"", paste(colnames(df), collapse = "\",\""))
    html <- sub("<<<COLUMNS>>>", col_names, html, fixed = TRUE)
    
    # ADD DATA
    json_data <- df_to_json_array(df)
    html <- sub("<<<DATA>>>", json_data, html, fixed = TRUE)
    
    return(html)
} 
