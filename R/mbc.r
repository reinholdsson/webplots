#' NVD3 multiBarChart
#'
#' ...
#'
#' @param df data frame
#'
#' @export
#'
mbc <- function(df, key = "key", x = "x", y = "y") {
    #html <- files_to_html(search_files(file.path(system.file(package = "webplots"), "deps", "mbhc"), c("css", "js", "html")))
    html <- files_to_html(search_files("inst/deps/mbc", c("css", "js", "html")))

    colnames(df)[colnames(df) == key] <- "key"
    colnames(df)[colnames(df) == x] <- "x"
    colnames(df)[colnames(df) == y] <- "y"

    df_json <- ddply(df, .(key), function(x) {
        df_to_json_array(x[c("x", "y")])
    })
    
    
    
    json <- sprintf("{key: '%s', values: %s}", df_json$key, df_json$V1)
    json <- paste(json, collapse = ",\n")

    html <- sub("<<<data>>>", json, html, fixed = TRUE)
    
    return(html)
}
