#' Run examples
#'
#' ...
#'
#' @param app App name
#'
#' @export
#'
run_example <- function(app = "parallel-sets") {
    if (app == "parallel-sets") {
        source(file.path(system.file(package = "webplots"), "examples", "parallel-sets.r"))
    } else if (app == "sankey") {
        source(file.path(system.file(package = "webplots"), "examples", "sankey.r"))
    } else if (app == "mbhc") {
        source(file.path(system.file(package = "webplots"), "examples", "mbhc.r"))
    }
} 
