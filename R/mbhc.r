#' NVD3 multiBarHorizontalChart
#'
#' ...
#'
#' @param df data frame
#' @param key Name of series
#' @param label Name of groups
#' @param color Series colors
#' @param show_values Show values? ('true'/'false')
#' @param tooltips Show tooltips? ('true'/'false')
#' @param axis_decimals Number of decimals in axis label (default = 0)
#'
#' @export
#'
mbhc <- function(df, key = colnames(df), label = rownames(df), color = rep("#d62728", ncol(df)), show_values = "false", tooltips = "true", axis_decimals = 0) {
    html <- files_to_html(search_files(file.path(system.file(package = "webplots"), "deps", "mbhc"), c("css", "js", "html")))
    
    json <- sapply(1:ncol(df), function(i) {
        sprintf("{key: '%s', color: '%s', values: %s}", key[i], color[i], df_to_json_array(data.frame(label = label, value = df[[i]])))
    })
    
    json <- paste(json, collapse = ",\n")
    
    html <- sub("<<<data>>>", json, html, fixed = TRUE)
    html <- sub("<<<showValues>>>", show_values, html, fixed = TRUE)
    html <- sub("<<<tooltips>>>", tooltips, html, fixed = TRUE)
    tick_format <- sprintf(",.%sf", axis_decimals)
    html <- sub("<<<tickFormat>>>", tick_format, html, fixed = TRUE)
    
    return(html)
} 
