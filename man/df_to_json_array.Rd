\name{df_to_json_array}
\alias{df_to_json_array}
\title{Data frame to JSON array}
\usage{
  df_to_json_array(df)
}
\arguments{
  \item{df}{data frame}
}
\description{
  Converts a data frame to a JSON array
  http://theweiluo.wordpress.com/2011/09/30/r-to-json-for-d3-js-and-protovis/
}

