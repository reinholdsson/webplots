webplots
========

## WORK IN PROGRESS

There are a lot of bugs in the nvd3 plots at the moment.

## INSTALL

Use `devtools` for easy installation

    library(devtools)

    # Install and load webplots
    install_github('webplots', 'reinholdsson')
    library(webplots)

